#ifndef OBJIO_HPP
#define OBJIO_HPP

#include <vector>
#include <string>
#include "glm.hpp"


std::ostream& operator<< (std::ostream& os, glm::vec3 v);

namespace obj
{
  void read(std::vector<glm::vec3>& verts, std::vector<glm::vec3>& norms, std::vector<unsigned int>& indices, const std::string& filename);
  void write(const std::vector<glm::vec3>& verts, const std::vector<glm::vec3>& norms, const std::vector<unsigned int>& indices, const std::string& filename);
}


#endif
