#include "bendingFunction.hpp"

#include "gtc/matrix_inverse.hpp"
#include "gtc/matrix_transform.hpp"

using glm::vec3;
using glm::vec4;
using glm::mat3;
using glm::mat4;

using std::vector;


void findBounds(vec3& out_min, vec3& out_max, const vector<vec3> verts)
{
  using std::min;
  using std::max;

  float inf = std::numeric_limits<float>::infinity();
  out_min = vec3(inf, inf, inf);
  out_max = vec3(-inf, -inf, -inf);

  for (auto v : verts) {
    out_min.x = min(v.x, out_min.x);
    out_min.y = min(v.y, out_min.y);
    out_min.z = min(v.z, out_min.z);

    out_max.x = max(v.x, out_max.x);
    out_max.y = max(v.y, out_max.y);
    out_max.z = max(v.z, out_max.z);
  }
}

//https://glm.g-truc.net/0.9.4/api/a00206.html#ga9a123708926335a4c097eee1157d4791
mat4 rotationMatrix(const vec3& v, const vec3& midpoint, float angle)
{
  static const mat4 identity = glm::mat4(1.0f);
  vec3 rotationCenter = vec3(midpoint.x, 0, midpoint.z);
  mat4 M1 = glm::translate(glm::mat4(1.0f), -rotationCenter);
  mat4 M2 = glm::rotate(glm::mat4(1.0f), angle, vec3(0,1,0));
  mat4 M3 = glm::translate(glm::mat4(1.0f), rotationCenter);

  return M3 * M2 * M1;
}

namespace bender
{
  void bend( vector<vec3>& verts_inout, vector<vec3>& norms_inout, float angle)
  {
    vec3 min;
    vec3 max;
    findBounds(min, max, verts_inout);
    vec3 midpoint = (max+min)/2.f;

    float interpLength = max.x - midpoint.x;

    for (unsigned long ii = 0; ii < verts_inout.size(); ++ii)
    {
      vec4 p = vec4(verts_inout[ii], 1.0);
      vec3 n = norms_inout[ii];

      if (p.x <= midpoint.x)
        continue;


      float f = (p.x - midpoint.x) / interpLength;
      float angle_ = angle * f;

      mat4 M = rotationMatrix(vec3(p), midpoint, angle_);
      mat3 M_n = mat3(glm::inverseTranspose(M_n));

      verts_inout[ii] = vec3(M * p);
      norms_inout[ii] = normalize(M_n * p);
    }
  }
}
