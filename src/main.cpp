#include <iostream>

#include <vector>

#include "objIO.hpp"
#include "bendingFunction.hpp"

#include "glm.hpp"

using namespace std;

using glm::vec3;

int main(int argc, char* argv[])
{
  if (argc < 4)
  {
    cerr << "ERROR: Not enough arguments" << endl;
    exit(1);
  }
  string input = string(argv[1]);
  float angle = atof(argv[2]) * 3.141592653589793 / 180.0;
  string output = string(argv[3]);

  cout << "bending " << input << " by " << angle << " radians\n";
  vector<vec3> verts;
  vector<vec3> norms;
  vector<vec3> norms_cpy;

  vector<unsigned int> tris;

  obj::read(verts, norms, tris, input);

  bender::bend(verts, norms, angle);

  obj::write(verts, norms, tris, output);

  return 0;
}
