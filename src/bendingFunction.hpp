#ifndef BENDINGFUNCTION_HPP
#define BENDINGFUNCTION_HPP

#include <vector>
#include "glm.hpp"

namespace bender
{
  //Angle in radians
  void bend(std::vector<glm::vec3>& verts_inout, std::vector<glm::vec3>& norms_inout, float angle);
}

#endif //BENDINGFUNCTION_HPP
