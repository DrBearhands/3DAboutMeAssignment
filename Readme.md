# Readme
Example assignment made for job application at 3D About Me. For assignment specifications see Assignment.md

## Used libraries
* .obj loader: https://github.com/Bly7/OBJ-Loader
* glm: https://glm.g-truc.net/0.9.9/index.html

## Limitations
* Only works for the most simple .obj files with just vertices, normals and indices (no meshes, no materials, no splines, etc.) as a full obj reader/writer was outside the scope of the assignment.

## Building
* On Linux `make`
* On Windows with MinGW: `mingw32-make`

Might complain about missing bin/ or build/ directories. Just create the directories to resolve.

## Usage
from `bin/` run `assignment <input-path> <angle> <output-path>`. With input path the path to .obj file to be bent, angle in degrees and output-path the path where to save the created .obj file (will overwrite without prompting).
